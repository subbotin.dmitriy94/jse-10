package com.tsconsulting.dsubbotin.tm.component;

import com.tsconsulting.dsubbotin.tm.api.controller.ICommandController;
import com.tsconsulting.dsubbotin.tm.api.controller.IProjectController;
import com.tsconsulting.dsubbotin.tm.api.controller.ITaskController;
import com.tsconsulting.dsubbotin.tm.api.repository.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.ICommandService;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.constant.ArgumentConst;
import com.tsconsulting.dsubbotin.tm.constant.TerminalConst;
import com.tsconsulting.dsubbotin.tm.controller.CommandController;
import com.tsconsulting.dsubbotin.tm.controller.ProjectController;
import com.tsconsulting.dsubbotin.tm.controller.TaskController;
import com.tsconsulting.dsubbotin.tm.repository.CommandRepository;
import com.tsconsulting.dsubbotin.tm.repository.ProjectRepository;
import com.tsconsulting.dsubbotin.tm.repository.TaskRepository;
import com.tsconsulting.dsubbotin.tm.service.CommandService;
import com.tsconsulting.dsubbotin.tm.service.ProjectService;
import com.tsconsulting.dsubbotin.tm.service.TaskService;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        commandController.displayWelcome();
        parseArgs(args);
        process();
    }

    private void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        commandController.exit();
    }

    private void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            default:
                commandController.displayArgError();
                break;
        }
    }

    private void process() {
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    private void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.INFO:
                commandController.displayInfo();
                break;
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            default:
                commandController.displayCommandError();
                break;
        }
    }

}
