package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
