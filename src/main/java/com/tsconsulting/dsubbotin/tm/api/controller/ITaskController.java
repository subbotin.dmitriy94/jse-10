package com.tsconsulting.dsubbotin.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
