package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

}
