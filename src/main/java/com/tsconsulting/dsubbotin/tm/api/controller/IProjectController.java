package com.tsconsulting.dsubbotin.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
