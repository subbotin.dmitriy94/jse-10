package com.tsconsulting.dsubbotin.tm.controller;

import com.tsconsulting.dsubbotin.tm.api.controller.IProjectController;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[Create]");
    }

    @Override
    public void showProjects() {
        if (projectService.findAll().isEmpty()) System.out.println("[List projects empty]");
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) System.out.println(project);
    }

    @Override
    public void clearProjects() {
        projectService.clear();
        System.out.println("[Clear]");
    }

}