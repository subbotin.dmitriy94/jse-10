package com.tsconsulting.dsubbotin.tm.controller;

import com.tsconsulting.dsubbotin.tm.api.controller.ITaskController;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("Enter name:");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[Create]");
    }

    @Override
    public void showTasks() {
        if (taskService.findAll().isEmpty()) System.out.println("[List tasks empty]");
        final List<Task> tasks = taskService.findAll();
        for (Task task : tasks) System.out.println(task);
    }

    @Override
    public void clearTasks() {
        taskService.clear();
        System.out.println("[Clear]");
    }

}